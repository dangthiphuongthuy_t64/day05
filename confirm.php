<!DOCTYPE html>
<html lang='en'>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="style.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js">
    </script>

    <title>Register</title>
</head>

<body>
    <?php
    session_start();
    ?>
    <form method="post" enctype="multipart/form-data" action="">
        <fieldset class="register-form">
            <div class="form">
                <div class="title">
                    <div class="input-text">
                        Họ và tên<sup class="sup">*</sup></div>
                    <div>
                        <?php echo $_SESSION['fname']; ?>
                    </div>
                </div>

                <div class="title">
                    <div class="input-text">
                        Giới tính<sup class="sup">*</sup></div>
                    <div>
                        <?php echo $_SESSION['gender']; ?>
                    </div>

                </div>

                <div class="title">
                    <div class="input-text">
                        Phân khoa<sup class="sup">*</sup></div>
                    <div>
                        <?php echo $_SESSION['department']; ?>
                    </div>
                </div>

                <div class="title">
                    <div class="input-text">Ngày sinh<sup class="sup">*</sup></div>
                    <div>
                        <?php echo $_SESSION['birthDate']; ?>
                    </div>
                </div>

                <div class="title">
                    <div class="input-text">
                        Địa chỉ</div>
                    <div>
                        <?php echo $_SESSION['address']; ?>
                    </div>
                </div>

                <div class="title">
                    <div class="input-text">
                        Hình ảnh</div>
                    <div>
                        <?php echo '<img src="' . $_SESSION["image"] . '" alt="Avatar" width="150" height="100">'; ?></br>
                    </div>
                </div>

                <input type='submit' class="button" name="register" value='Xác nhận' />
            </div>

        </fieldset>
    </form>
    <script type="text/javascript">
        $(function() {
            $('#datepicker').datepicker();
        });
    </script>
</body>

</html>