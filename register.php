<!DOCTYPE html>
<html lang='en'>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="style.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js">
    </script>

    <title>Register</title>
</head>

<body>
    <?php
    session_start();
    // Code PHP xử lý validate
    $error = array();
    $data = array();
    $action = '';
    $date = date("YmdHis");
    $_SESSION = $_POST;

    if (!empty($_POST['register'])) {
        // Lấy dữ liệu
        $_SESSION['fname'] = isset($_POST['fname']) ? $_POST['fname'] : '';
        $_SESSION['gender'] = isset($_POST['gender']) ? $_POST['gender'] : '';
        $_SESSION['department'] = isset($_POST['department']) ? $_POST['department'] : '--Department--';
        $_SESSION['birthDate'] = isset($_POST['birthDate']) ? $_POST['birthDate'] : '';

        // Kiểm tra định dạng dữ liệu
        if (empty($_POST['fname'])) {
            $error['fname'] = 'Hãy nhập tên.';
        }

        if (empty($_POST['gender'])) {
            $error['gender'] = 'Hãy chọn giới tính';
        }

        if (empty($_POST['department']) || $_POST['department'] == '--Department--') {
            $error['department'] = 'Hãy chọn phân khoa.';
        }

        if (empty($_POST['birthDate'])) {
            $error['birthDate'] = 'Hãy nhập ngày sinh';
        }

        //Thư mục bạn sẽ lưu file upload
        $target_dir    = "images/";
        if (!file_exists($target_dir)) {
            mkdir($target_dir);
        }
        $filename = $_FILES["image"]["tmp_name"];
        $prod = "img";
        $extension = pathinfo(basename($_FILES["image"]["name"]), PATHINFO_EXTENSION); // jpg
        $newfilename = $prod . "_" . $date . "." . $extension;
        //Vị trí file lưu tạm trong server (file sẽ lưu trong uploads, với tên giống tên ban đầu)
        $target_file   = $target_dir . basename($newfilename);

        // Lưu dữ liệu
        if (empty($error)) {
            move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
            $_SESSION['image'] = $target_file;
            header("Location: ./confirm.php ");
        }
    }
    ?>
    <form method="post" action="" enctype="multipart/form-data" action=?#?>
        <fieldset class="register-form">
            <div class="form">
                <?php echo isset($error['fname']) ? $error['fname'] : ''; ?> <br />
                <?php echo isset($error['gender']) ? $error['gender'] : ''; ?> <br />
                <?php echo isset($error['department']) ? $error['department'] : ''; ?> <br />
                <?php echo isset($error['birthDate']) ? $error['birthDate'] : ''; ?> <br />

                <div class="title">
                    <div class="input-text">
                        Họ và tên<sup class="sup">*</sup></div>
                    <input type='text' id='fname' name='fname' class="select">

                </div>

                <div class="title">
                    <div class="input-text">
                        Giới tính<sup class="sup">*</sup></div>
                    <?php
                    $Gender = array("Nam", "Nữ");
                    $keys = array_keys($Gender);
                    for ($i = 0; $i <= count($Gender) - 1; $i++) { ?>
                        <input type="radio" name="gender" checked="<?php echo "checked"; ?>" value=" <?php echo $Gender[$i]; ?>"> <?php echo $Gender[$i]; ?>
                    <?php } ?>
                </div>

                <div class="title">
                    <div class="input-text">
                        Phân khoa<sup class="sup">*</sup></div>
                    <select class="select" name="department">
                        <option>--Department--</option>
                        <?php $department = array("null" => " ", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học dữ liệu");
                        foreach ($department as $key => $value) { ?>
                            <option value="<?= $value ?>"><?= $value ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="title">
                    <div class="input-text">Ngày sinh<sup class="sup">*</sup></div>
                    <div id="datepicker" class="input-group date" data-date-format="dd-mm-yyyy">
                        <input class="form-control" readonly="" type="text" name="birthDate">
                        <span class="input-group-addon">
                        </span>
                    </div>
                </div>

                <div class="title">
                    <div class="input-text">
                        Địa chỉ</div>
                    <input type='text' id='address' name='address' class="select">
                </div>

                <div class="title">
                    <div class="input-text">
                        Hình ảnh</div>
                    <input type='file' id='image' name='image' accept=".jpg,.jpeg,.png">
                </div>

                <input type='submit' class="button" name="register" value='Đăng ký' />
            </div>

        </fieldset>
    </form>
    <script type="text/javascript">
        $(function() {
            $('#datepicker').datepicker();
        });
    </script>
</body>

</html>